---
title: Sonnwendfeier 2022
date: 2022-06-22
---
### Das erste Mal seit dem Beginn der Corona-Pandemie

Bei sonnigem und warmem Wetter trafen wir uns auf dem Gelände des 1.&nbsp;MYC
Nürnberg, um gemeinsam bei Essen und Getränken gemütlich zusammenzusitzen. Mit
der Dunkelheit wurde ein Feuer angeschürrt, dass später mit etwas Nachhilfe
richtig gut brannte.

Endlich wieder Sonnwendfeiern!
