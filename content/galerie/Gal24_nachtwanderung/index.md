---
title: Nachtwanderung 2024
date: 2024-11-09
---
### Auf Geisterwegen durch Nürnbers Altstadt

Mit 50 Teilnehmern war die Wanderung vollständig ausgebucht. Unter fachkundiger Führung ging es ein einhalb Stunden mit Start am Schönen Brunnen durch die Altstadt.
Fachkundige Führung erzählte bei kurzweiligen Stopps an historischen Orten Bekanntes und Unbekanntes aus der Zeit des Mittelalters. Eine gelungene Veranstaltung!
