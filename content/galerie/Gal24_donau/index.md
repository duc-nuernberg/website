---
title: Sommer-Donauschwimmen 2024
date: 2024-07-14
---
### Genau richtig

Besser konnten wir es nicht treffen. Sonne, blauer Himmel und eine angenehme Wassertemperatur. Mit dem Schiff ging es von Kehlheim die Donau flußaufwärts bis zum Kloster Weltenburg. Im Neopren mit Flossen unter dem Arm - versteht sich. Von dort noch einen kleiner Fußmarsch bis zum Einstieg.

Den Weg zurück nach Kehlheim erledigte die Strömung. Bei kleinen Halten konnten die Personenschiffe ungehindert passieren. 

Zurück in Kehlheim ging es nach dem Trockenlegen in unser "Stamm-"Brauhaus, wo wir unser diesjähriges Donauschwimmen gemütlich ausklingen ließen.