---
title: Donau(Sommer)schwimmen
date: 2023-07-01
---
Hatten wir es im letzten Jahr gerade noch rechtzeitig zwischen den
"Hoch"wasserständen mit dem Schwimmen geschafft, war es heuer kein Problem.
Ausreichend hohes Wasser, sichere Schifffahrt und ein Wettergott, der es dieses
Mal gut mit uns meinte. Die Teilnehmerzahl können wir im nächsten Jahr
hoffentlich etwas nach oben steigern.

Der Himmel riss bereits vor dem Ablegen immer mehr auf und die Sonne versprach
ein schönes Donau-Abwärts-Treiben. Zugegeben die Blicke der "Touris" sind jede
Jahr vielsagend, wenn wir in Neopren gewandet und den Flossen unter dem Arm das
Schiff besteigen. Noch schnell ein Getränk und die Aussicht genießen, bevor wir
am Kloster Weltenburg das Schiff verlassen und ein Stückchen flussauf zum
Startpunkt wandern.

Finales Anziehen und rein ins Wasser. Erfreulicher Weise war es überhaupt nicht
kalt und die Strömung genau richtig, um nach einer knappen Stunde wieder in
Kehlheim wohlbehalten anzulanden. Nachdem alle umgezogen waren, ging es
traditionell ins "Weissen Bräuhaus" zum Essen und - wer noch mochte - eine
Portion Eis auf dem Heimweg.
