---
title: Antauchen 2024
date: 2024-05-09
---
### Außen warm und unter Wasser frisch

Traumhaftes Wetter mit strahlendem Sonnenschein und blauem Himmel. Außen mussten wir aufpassen, dass wir uns keinen Sonnenstich holten. 
Unter Wasser war es noch ziemlich frisch und ein Trocki die richtige Entscheidung, auch wenn jeder froh war, wenn er nach Anrödeln und Body-Check das Wasser erreichte.
Aber so konnten wir alle die natürlichen und künstlichen Schönheiten des Sundhäuser Sees in vollen Zügen genießen.

