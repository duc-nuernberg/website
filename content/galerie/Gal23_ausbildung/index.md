---
title: Ausbildung 2023
date: 2023-05-01
---
### Impressionen von der Ausbildung 2023

1-Stern, Rettungstraining, Aufbaukurse "Orientierung beim Tauchen" und
"Gruppenführung". Wir haben in der ersten Jahreshälfte wieder Einiges angeboten
und die Teilnehmer konnten sich neues Wissen aneignen oder vertiefen. Unten
sind ein paar Foto-Impressionen.
