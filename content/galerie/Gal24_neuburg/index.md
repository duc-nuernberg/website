---
title: Winter-Donauschwimmen 2024
date: 2024-01-01
---
### Das 53. Donauschwimmen in Neuburg - rasant und kurz!

Zwei Tage vorher mussten sich die Neuburger Veranstalter wegen der hohen
Fließgeschwindigkeit zu einer abgewandelten Variante für die Schwimmer
entscheiden. Die Strecke wurde verkürzt und ohne Großgeräte geschwommen.
Zugelassen waren lediglich Geräte, die von den Schwimmern selbst aus dem Wasser
gebracht werden konnten. Alles besser als Absagen! Nur keiner von uns DUClern
hatte das mitbekommen! So standen wir mit unserem DUC-Mobil an der Staustufe
vor einer verschlossenen Schranke. Weit und breit keine Feuerwehr oder
Wasserwacht, die sonst zu dieser frühen Stunde bereits mit den Vorbereitungen
beschäftigt waren. Zum Glück brachte der Anruf unserer Vereinskollegen, die
sich um die Anmeldung vor Ort kümmerten, Klarheit.

Letztendlich trafen sich alle in der Schwimmhalle zum Umziehen, Kostümieren und
Einstimmen.  Viele von unseren diesjährigen Vereinsteilnehmern waren zum ersten
Mal dabei und erstaunt und beeindruckt vom bunten Schwimmvolk, den vielen
Zuschauern, der Stimmung und dem, was die Wasserwacht da auf die Beine stellt.
Die Pendelbusse brachten alle Schwimmer zur Brandlwiese.

Das Schwimmen ging heuer zügig von statten. Keine Zeit, um kalte Finger und
Füße zu bekommen. Egal ob im Trocki, dickem Taucherneopren oder Apnoe-Anzug. 10
Minuten Schwimmzeit meinte die Presse. Helfende Hände griffen wo nötig zu, um
uns alle wieder sicher an Land zu bringen. Die heiße Suppe stand genauso
bereit, wie die Feuerwehr, die jeden grob vorreinigte, bevor es wieder ins
Schwimmbad ging. Duschen, enges Kuscheln im wohl temperierten Schwimmbecken
inklusive. Der Ausklang fand mit Essen, Getränken und Rahmenprogramm in der
großen Festhalle statt. Die Großgeräte waren auf dem Parkplatz ausgestellt und
wurden wie in jedem Jahr prämiert. Nur für den Festball hatten wir auch diesmal
keine Karten.

Vielleicht nächstes Jahr?
