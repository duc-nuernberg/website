---
title: Sonnwendfeier 2023
date: 2023-06-22
---
### Zwar ohne Feuer, aber mit Essen, Getränken und Musik

Auch diese Jahr haben wir mit dem Wetter alles richtig gemacht. Sonne pur,
klarer Himmel und ein lauer Abend. Genau das Richtige, um sich auf dem Gelände
des 1.&nbsp;MYC Nürnberg zu treffen. Für Essen, Getränke und Musik war gesorgt.
Leider konnten wir heuer kein Feuer entzünden, da viele Boote des 1.&nbsp;MYCs
noch auf dem Trocknen lagen und das Risiko von übergreifenden Flammen zu groß
war.

Vielleicht nächstes Jahr wieder.
