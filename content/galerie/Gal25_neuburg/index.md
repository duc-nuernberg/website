---
title: Winter-Donauschwimmen 2025
date: 2025-01-25
---
### Party-Spaß auf der langen Strecke

Nachdem im letzten Jahr wegen zu viel Wasser die Strecke deutlich verkürzt werden musste, konnten wir dieses Mal wieder die volle Distanz genießen. BRK und Wasserwacht Neuburg keine Mühen gescheut, um ein durchweg gelungenes und sicheres Event zu organisieren. Wir möchten uns herzlich bei Walter Brendel bedanken, der nach so vielen Jahren als Chef-Organisator, den Staffelstab im kommenden Jahr an seinen Nachfolger weiterreichen wird.

Auch wenn wir mit nur fünf Teilnehmern unsere siebte Teilnahme bestritten, tat das dem Spaß keinen Abbruch. Wie die Bilder belegen, haben sich die Teams im Kampf um das beste Schwimmgefährt Einiges einfallen lassen. Den Sieger konnten wir im Bild festhalten. Zugegeben, unsere DUC-Bar kann da nicht mithalten. Aber sie wird jedes Jahr verbessert und versorgt uns während des Schwimmens bestens mit heißen Getränken.

Wir wissen nicht genau, ob es daran lag, dass wir erst kurz vor dem Start an der Staustufe ankamen und ohne vom Warten kalten Füßen direkt ins Wasser gingen. Auf jeden Fall waren nach 60 Minuten schwimmen lediglich die Finger ein Wenig kalt. Die heiße Suppe, das gut geheizte Bad - teilweise mit eng gepacktem Herings-Feeling - und die warmen Duschen waren wie immer Wohltat und Spaß zugleich. Und später gab es in der großen Festhalle Essen, Trinken und Musik zum Ausklang. Zumindest für uns und alle, die leider keine Karte für den Schwimmer-Ball hatten.