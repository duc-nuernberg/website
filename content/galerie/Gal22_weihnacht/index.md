---
title: Weihnachtsfeier 2022
date: 2022-12-24
---
### Weihnachtsfeier mal anders

Dank Dalibor und Sohn Steffen konnte unser Vorstand im Dezember eine Lokalität
mit ausreichendem Platz finden, wo sich unsere Vereinsmitglieder in der
Vorweihnachtszeit noch einmal treffen konnten. Bei leckeren italienischen
Gerichten, weihnachtlichen Süßigkeiten und Getränken ließen wir das Jahr 2022
ausklingen.
