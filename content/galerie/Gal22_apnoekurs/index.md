---
title: Apnoe Freediving Kurs 2022
date: 2022-04-30
---
Am 30.04.2022 ist die DUC Nürnberg e.V. Freediving Truppe größer geworden. Bei
strahlendem Sonnenschein und Unterwassersicht weit über 10 Meter fand die
Freediving Ausbildung im Open Water und Brevet Abnahme am Murner See statt. Das
8 Grad kalte Wasser war einerseits eine Herausforderung für die Anfänger
anderseits eine richtige Freediving Taufe, die sie alle gut bestanden.

Kamila Muhammad beendete mit den Rettungsübungen aus der Tiefe ihren Crossover
Kurs und Victoria Saft und Tanja Hunger schließen nach der Pool Ausbildung die
Freediving Ausbildung im Open Water ab. Somit stieg die Frauenquote nach diesem
Kurs in der DUC Nürnberg e.V. Freediving Truppe rasant auf.

Vielen Dank auch an Georg Montag (Apnoe \*\*) für die Unterstützung beim Kurs.
Und wir sehen uns im Südstadtbad beim Training mittwochs und freitags.

@Vladimir Poliakov, 30.04.2022
