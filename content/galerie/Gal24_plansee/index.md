---
title: Plansee-Event 2024
date: 2024-06-22
---
### Traumhaft mit viel Wasser

Wie organisieren unsere Tauchfreunde vom SC53 Landshut ein Mega-Event? Man(Frau) bucht über ein Jahr im Voraus das Plansee-Camp.
Organisiert Ausbilder für Tauchscheine, Nachttauchen und Scooter. Schickt eine Vielzahl von Mails und WhatsApp-Nachrichten,
um jeden an die zum Tauchen erforderlichen Unterlagen, die Kursanmeldungen etc. zu erinnern und über das Event zu informieren.

Selbst beim Wettergott wurde ein gutes Wort eingelegt, sodaß sich Sonne und Regen vernüftig abwechselten. Die Sicht war wegen
des vielen Regens im Vorfeld etwas eingetrübt, aber händelbar. Nichts was Taucher davon abschreckt.

Dank freier Plätze konnte eine Gruppe von unseren Mitgliedern an dem Event teilnehmen. Wir hatten eine Riesen-Spaß, konnten
Neues lernen, uns austauschen und sowohl Landschaft als auch See genießen.

Vielen Dank nochmal an die Landshuter Tauchfreunde!
