---
title: German Diver Licence * / CMAS-Taucher * - Ausbildung 2022
date: 2022-05-21
---
### Freiwassertauchgänge am Murner See für die CMAS\* Prüfung

Nach dem wir in den kalten Monaten im Südstadtbad die ABC-Ausbildung und erste
Erfahrungen mit dem Tauchgerät machen konnten, ging es am Wochenende vom
21-22.05.2022 an den Murner See, um unsere Freiwassertauchgänge zu machen. Bei
schönstem Wetter ging es um 11 Uhr los. Alle CMAS\* Schüler und Tauchlehrer
rückten mit viel Gepäck an. Dieses musste erstmal vom Parkplatz zum See
geschleppt werden. Jetzt weiß ich, was der Spruch „Ein Taucher taucht sich
nicht tot – er schleppt sich tot“ bedeutet. Tauchflaschen, Tarierwesten,
Neoprenanzüge, Atemregler, Verpflegung usw., aber jeder hat fleißig mitgeholfen
und unser Tauchlager war schnell eingerichtet.

Zunächst bekamen wir eine kurze Einweisung unserer Tauchlehrer, was uns die
nächsten Tage alles so erwartet. Dann ging es auch schon los. Rein in die
Tauchklamotten und auf zum ersten Tauchgang. Wenn ich von mir spreche, kann ich
sagen, dass ich schon etwas nervös und ängstlich war. Durch gutes Zureden und
viel Verständnis der Tauchlehrer legte sich dies aber schnell. Die Gruppen
wurden aufgeteilt und dann ging es rein in den 13 Grad warmen bzw. kalten See.
Beim ersten Tauchgang sollten wir noch keine Prüfungen machen und uns erstmal
an alles gewöhnen. Als nach ca. 15 Minuten alle wieder an der Wasseroberfläche
„aufpoppten“, hatte jeder auf seine Weise einen ersten Eindruck vom Tauchen in
einem See. Um sich das mühevolle An- und Ablegen der Tauchausrüstung zu
ersparen, beschlossen unsere Tauchlehrer den zweiten Tauchgang – ohne den See
zu verlassen – gleich im Anschluss durchzuführen. Nun versuchten wir, besseres
Tarieren zu üben. Dazu konnten im See angebrachte Übungsringe genutzt werden.
Vor dem dritten Tauchgang sollten wir alle erstmal Pause machen. Also raus aus
dem Wasser zurück zu unserem Tauchlager. Tauchausrüstung ablegen – trockene
Sachen anziehen - essen u. trinken und dann ausruhen. Nach einer längeren Pause
hieß es dann wieder „Also quetscht euch nochmal rein in eure Tauchsachen“. Beim
dritten Tauchgang wollten wir etwas tiefer tauchen als bei den ersten
Tauchgängen, außerdem wurden bei diesem Tauchgang alle Unterwasserzeichen
abgefragt, die ein Taucher können muss. Auch meine Lieblingsdisziplin „Maske
ausblasen“ wurde nochmals wiederholt. Das war Tag eins unserer
Freiwassertauchgänge und jeder ging zufrieden aber auch erschöpft nach Hause.

Am Sonntag ging es wieder um 11 Uhr los. An diesem Tag hatten unserer Ausbilder
zwei Tauchgänge geplant. Beim ersten Tauchgang sollten wir eine Rettungsübung
durchführen und eine verunfallte Person aus etwa 6m Tiefe sicher an die
Wasseroberfläche bringen. Der genaue Ablauf der Übung wurde uns vorab gut
erklärt. Wieder an Land übten wir den weitern Ablauf der Rettungskette sowie
die stabile Seitenlage und Wiederbelebung einer verunfallten Person. Beim
zweiten Tauchgang sind wir auf ca. 15m Tiefe gegangen – wo die Kälte doch
spürbar zunahm und man froh um all die mühsam angelegte Schutzkleidung war.
Noch unter Wasser wurden wir von unseren Tauchlehrern zu unserem bestandenen
CMAS\* beglückwünscht. Zurück an Land waren wir froh und stolz, nun den
Tauchschein in der Tasche zu haben.

Ein großer Dank geht an dieser Stelle an unser Ausbildungsteam Maria, Steffen
und Stefan, die uns sicher und mit viel Freude auf alle Prüfungen super
vorbereitet haben.

Eure Taucheinsteiger bzw. jetzigen CMAS\*er \
Volker, Lorenz, Nikolaj, Luke und Kathrin  ääääh Karin ;)
