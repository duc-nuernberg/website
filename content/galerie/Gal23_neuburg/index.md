---
title: Winter-Donauschwimmen 2023
date: 2023-01-01
---
### Nach zwei Jahren Corona-Pause wieder: Das 52. Donauschwimmen in Neuburg!

Zwei Jahre musste es leider Corona-bedingt ausfallen: Das Winter-Donauschwimmen
in Neuburg. Doch dieses Jahr hat es wieder geklappt. Kein Packeis auf dem
Fluss. Wasserstand und Fließgeschwindigkeit im Normbereich. Nur etwas
Rest-Schnee mit leider bedecktem Himmel. Außer dass der Veranstalter den
Donauball nicht organisieren durfte, gab es keine Einschränkungen.

Parken konnten wir im neuerrichteten Parkhaus direkt neben der Schwimmhalle.
Auch die Vor- und Nachverpflegung und das gemütliche Beieinander nach dem
Schwimmen war hier organisiert. An der Organisation gab es wie vor Corona
Nichts auszusetzen. Im "Faschings"-Neopren ging es mit Flossen per Pendelbus an
den Start, wo wir vorab unsere "DUC-Bar" hingeschafft hatten. Mit dem
Startschuss brachen alle Teilnehmer mit ihren Flößen, Ringen, Gummi-Enten etc.
nach Neuburg auf. Etliche Wasserwachtler und Vereine hatten sich wieder mächtig
ins Zeug gelegt, um ausgefallene Gefährte zu Wasser zu lassen.

Nach "Arschbomben", Party-Musik und Rumablern bekamen wir noch am Ufer heißen
Tee und Suppe zum Aufwärmen, wurden vor der Schwimmhalle vom groben Schmutz
befreit. In der Schwimmhalle taten Duschen und aufgeheißte Becken ihr Übriges,
um Hände und Füße wieder mit Gefühl zu versorgen, bevor man sich in der Halle
zu Essen, Kaffee, Kuchen mit Musik traf und das Donauschwimmen 2023 ausklingen
ließ.
